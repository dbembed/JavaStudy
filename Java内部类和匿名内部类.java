什么是内部类：
内部类就是一个定义在一个类中的类。
下面B就是A的内部类
代码示例：

class A{
	
	class B{
		
	}
	
}

内部类的语法：

1. 如何生成内部类的对象

代码示例：

class Test{
	public static void main(String args[]){
		A a = new A();	//生产类对象
		A.B b= new A().new B();//生成A的内部类对象
	}
}

内部类可以使用外部类的成员变量和方法
内部类可以访问外部类的任何成员，包括private成员
外部类访问内部类的成员需要创建内部类的对象，之后可以访问内部类的任何成员，包括private成员，需要注意的是成员内部类不可以有静态成员。
当外部类的成员和内部类的成员重名时单单用this是区分不了的。在内部类中访问外部类的成员时可以用如下语法区分：
<外部类类名>.this.<外部类中需要被访问的成员名>

必须先有外部类的对象才能生成内部类的对象，因为内部类的作用就是为了访问外部类的成员变量。


内部类中的变量访问形式：
代码示例：
class Out{
	private int age = 12;
	
	class In{
		private int age = 13;
		
		public void print(){
			int age = 14;
			System.out.println("局部变量：" + age);
			System.out.println("内部类变量：" + this.age);
			System.out.println("外部类变量：" + Out.this.age);
		}
	}
}

匿名内部类

