向上转型(upcasting) 、向下转型(downcasting)
转型是在继承的基础上而言的。

1. 向上转型：将子类的对象赋值给父类的引用；
	
代码示例：
class Person{
	String name;
	int age;
	
	void introduce(){
		System.out.println("我的姓名是"+ name + "，我的年龄是" + age);
	}
}

class Student extends Person{
	String address;
	
	void introduce(){
		super.introduce();	
		System.out.println("我的家在" + address)
	}
	
	void study(){
		System.out.println("学生学习");
	}
}

class Test{
	Student s = new Student();
	Person p = s;
	
}

如上面：
	Student s = new Student();
	Person p = s;
	
把s赋值给p

一个引用能够调用哪些成员(变量和函数)，取决于这个引用的类型
一个引用调用的是哪一个方法，取决于这个引用所指向的对象

-------------------------------------------------
接口的引用指向实现它的类的对象
解释如下：接口的引用能够指向实现它的类的一个对象，然后通过这个接口的引用来
调用接口的属性和方法(调用的方法其实是实现了它的类的方法)
简单的例程：
//定义接口
interface MyInterface {
	//定义接口属性，默认的修饰符是public static
	String name = "我是接口属性"
	//接口方法
	String print();
}
//实现Myinterface接口的类
public class Test implements  MyInterface {
	@Override
	String print(){
		return "我实现了接口的方法"
	}

	public static void main(String arg[]) {
		//接口的引用指向了实现它的类的对象
		MyInterface my = new Test();
		//通过接口的引用调用接口的属性
		System.out.println(my.name);
		//上面这条语句写成下面这样会更好
		//因为Test实现了接口，所以也继承了接口中的属性
		//并且接口中定义的属性默认的修饰符是public static
		//所以通过类名可以直接调用
		System.out.println(Test.name);
		//这里的my.print()方法其实是
		//调用的实现了MyInterface接口的Test类的中的print方法
		System.out.println(my.print());
		
	}
}




2. 向下转型
向下转型：将父类的对象赋值给子类的引用
还用上的代码示意：
Student s1 = new Student();
Person p = s1;
Student s2 = (Student)p;
向下转型的第一步就要先向上转型；

总结：

1. 父类引用指向子类对象，而子类引用不能指向父类对象。

2. 把子类对象直接赋给父类引用叫upcasting向上转型，向上转型不用强制转换。

3. 把指向子类对象的父类引用赋给子类引用叫向下转型(downcasting)，要强制转换。
注意：转型中的父类对象可以是接口

转型的作用是什么？
