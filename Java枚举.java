Java 枚举的7中用法：
1. 常量
java代码示意
	public enum Color {
		RED,GREEN,BLANK,YELLOW
	}
	
2. 	switch 中使用
java代码示意
	enum Signal {
		GREEN,YELLOW,RED
	}
	public class TrafficLight {
		Signal color = Signal.RED;
		
		public void change() {
			switch (color){
				case RED:
					color = Signal.GREEN;
				case YELLOW:
					color = Signal.RED;
				case GREEN:
					color = Signal.YELLOW;
			}
		}
	}
	
3. 向枚举中添加新方法
如果打算自定义自己的方法，那么必须在enum实例序列的最后添加一个分号，而且Java要求必须先定义enum实例

Java代码示意
	public enum Color{
		RED("红色", 1), GREEN("绿色", 2), BLANK("白色", 3), YELLO("黄色", 4);
		//成员变量
		private String name;
		private int index;
		
		//构造方法
		private Color(String name, int index){
			this.name = name;
			this.index = index;
		}
		
		//普通方法
		public static String getName(int index){
			for(Color c : Color.values()){
				if (c.getIndex() == index){
					return c.name;
				}
			}
			return null;
		}
		
		//get set 方法
		public String getName() {
			return this.name;
		}
		public void setName(String name){
			this.name = name;
		}
		public int getIndex() {  
			return this.index;  
		}  
		public void setIndex(int index) {  
			this.index = index;  
		}  
	}